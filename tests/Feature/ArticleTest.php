<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAddArticle()
    {
        $response = $this->post(
            '/article',
            [
                'title' => 'Testing',
                'content' => 'Testing1111'
            ]
        );

        $response->assertRedirect('/article');
    }
}
